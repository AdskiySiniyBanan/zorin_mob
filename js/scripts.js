/**
 * Created by Admin on 04.10.2016.
 */
$(document).ready(function(){

    
    // <---blocks "variables"--->
    
    // <---normal variables--->


    // <---pre calculating--->

$(".mob-burger").click(function () {
   $(this).toggleClass("active");
    $(".mob-menu").fadeToggle();
});
    // var $ww=$(window).width(),
    //     $wow=$(window).outerWidth(),
    //     $whelp=$(".width-helper");
    // $whelp.text($ww+' - '+$wow);
    // $(window).resize(function () {
    //     var $wow=$(window).outerWidth();
    //         $ww=$(window).width();
    //    $(".width-helper").text($ww+' - '+$wow);
    // });
    // $whelp.click(function () {
    //     $(this).hide();
    // });
    var $news_extend=$(".extend-btn");
    $news_extend.click(function (event) {
        event.preventDefault();
        $(this).parents(".news-block").toggleClass("extended");
    });
    var $numbers=$(".counter-number"),
        $numbers_count=$(".counter-number").length,
        $number,
        q=0;
    if($numbers_count){
        setTimeout(function () {
            movenumber($numbers.eq($numbers_count-1),$numbers_count);
        },2150);
    }
    function movenumber($cur_number,quantity){
        $number=$cur_number.find(".counter-number-old").text();
        $cur_number.find(".counter-number-old").animate({bottom: "-100%"},1500);
        $cur_number.find(".counter-number-new").animate({bottom: "0"},1500);
        if($number==9){
            q++;
            var z=--quantity;

            if(z>0){
                setTimeout(function () {
                    movenumber($numbers.eq($numbers_count-1-q),z);
                },500);
            }
        }
    }
    $(".timeline-mask").animate({width:0},2500);
    var $timeline_points=$(".timeline-block");
    setTimeout(function () {
        timelineAppear(0);
    },1000);
    function timelineAppear(i){
        if(i<$timeline_points.length){
            $timeline_points.eq(i).fadeIn();
            setTimeout(function () {
                timelineAppear(i+1);
            },250)
        }
    }
    var $smooth_links=$(".smooth_scroll_link"),
        $smooth_anchor,
        anchor_string,
        anchor_scroll;
    $smooth_links.each(function () {
        $(this).click(function (event) {
            event.preventDefault();
            anchor_string=$(this).attr("href");
            $smooth_anchor=$(anchor_string);
            anchor_scroll=$smooth_anchor.offset().top;
            $("body, html").animate({scrollTop: anchor_scroll}, 800);
            $(".mob-burger").removeClass("active");
            $(".mob-menu").fadeOut();
        });
    });
});
/**
 * Created by Admin on 12.10.2016.
 */
$(document).ready(function(){
    var text_slides=$(".slider-description");
    var $main_slider = new Swiper('.main-slider', {
        slidesPerView: 1,
        speed: 2000,
        autoplay: 3000,
        effect: 'fade',
        spaceBetween: 30,
        nextButton: ".slider-btn-next",
        prevButton: ".slider-btn-prev",
        onSlideChangeStart: function(swiper){
            text_slides.fadeOut(500);
            setTimeout(function () {
                text_slides.eq($main_slider.activeIndex).fadeIn(500);
            },600);
        }
    });

    var $news_slider = new Swiper('.news-slider', {
        slidesPerView: 3,
        speed: 2000,
        effect: 'slide',
        spaceBetween: 15,
        touchRatio: 0,
        breakpoints: {
            // when window width is <= 480px
            500: {
                slidesPerView: 1,
                touchRatio: 1
            },
            // when window width is <= 640px
            660: {
                slidesPerView: 2,
                touchRatio: 1,
                spaceBetween: 30
            }
        },
        onSlideChangeStart: function(swiper){
            $(".pointer").fadeOut();
        }

    });
    var $comment_slider = new Swiper('.c-events-comments-slider', {
        slidesPerView: 2,
        speed: 2000,
        effect: 'slide',
        loop: "true",
        spaceBetween: 20,
        prevButton: ".c-events-comments-arrow-left",
        nextButton: ".c-events-comments-arrow-right",
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 30
            }
        }
    });
});
